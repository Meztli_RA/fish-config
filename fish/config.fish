# Fish config file

### Export path ###
set PATH $PATH /home/meztli/go/bin /home/meztli/.local/bin /home/meztli/.emacs.d/bin /home/meztli/.cargo/bin

### Aliases ###
# ls aliases 
alias ls="exa -l"
alias la="exa -la"
# cat aliases
alias cat="bat"
# grep aliases
alias grep="rg"
# ifetch aliases
alias Ifetch="~/ifetch/Ifetch.sh"

~/shell-color-scripts/colorscripts/colorbars
